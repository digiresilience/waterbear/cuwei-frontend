BUILD_DATE   ?=$(shell date -u +”%Y-%m-%dT%H:%M:%SZ”)
DOCKER_ARGS  ?=
DOCKER_NS    ?= registry.gitlab.com/digiresilience/waterbear/cuwei-frontend
DOCKER_TAG   ?= ${CUWEI_TAG}
DOCKER_BUILD := docker build ${DOCKER_ARGS} --build-arg BUILD_DATE=${BUILD_DATE}
DOCKER_BUILD_FRESH := ${DOCKER_BUILD} --pull --no-cache
DOCKER_BUILD_ARGS := --build-arg VCS_REF=${CI_COMMIT_SHORT_SHA}
DOCKER_PUSH  := docker push
DOCKER_BUILD_TAG := ${DOCKER_NS}:${DOCKER_TAG}

.PHONY: build build-fresh push build-push build-fresh-push clean

build: .npmrc
	${DOCKER_BUILD} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

build-fresh: .npmrc
	${DOCKER_BUILD_FRESH} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

push:
	${DOCKER_PUSH} ${DOCKER_BUILD_TAG}

build-push: build push
build-fresh-push: build-fresh push

add-tag:
	docker pull ${DOCKER_NS}:${DOCKER_TAG}
	docker tag ${DOCKER_NS}:${DOCKER_TAG} ${DOCKER_NS}:${DOCKER_TAG_NEW}
	docker push ${DOCKER_NS}:${DOCKER_TAG_NEW}

test: .npmrc
	npm install
	npm run build
	npx jest --runInBand --detectOpenHandles

.npmrc:
	echo '@digiresilience:registry=https://gitlab.com/api/v4/packages/npm/' > .npmrc
	echo '//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
