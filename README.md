## Architecture

We break the program into individual component files, so for example we can create our own file at `/components/MyComponent.js` and include it in `/components/App.jsx`.

The actual entry point for the javascript is `/App.jsx` but that simply loads React, our base CSS, and our `App` component and then renders the `App` component, so for the purposes of development, `/components/App.js` is the root of the site and controls and maintains state as needed.

## Image editor

This uses the [Toast UI React Image Editor](https://github.com/nhn/toast-ui.react-image-editor/). Right now it's rendering with their default UI but I think I'm going to use Evergreen components
