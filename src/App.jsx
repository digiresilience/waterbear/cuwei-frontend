import React from 'react';
import { HashRouter as Router } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { TopBar } from './TopBar.jsx';
import ApolloClient from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from '@apollo/react-hooks';
import { TicketProvider } from './TicketProvider.jsx';
import { AppContent } from './AppContent.jsx';
import ScrollToTop from './ScrollToTop.jsx';

const cache = new InMemoryCache({});
const client = new ApolloClient({
  credentials: 'include',
  link: new HttpLink({
    credentials: 'include',
    uri: '/graphql',
  }),
  cache,
});

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#009688',
      teal: '#009688',
      darkGray: '#4a4a4a',
      mediumGray: '#9a9a9a',
      lightGray: '#fbfbfb',
      royalBlue: '#3701b3',
      darkBlue: '#21006C',
    },
    secondary: {
      main: '#3701b3',
    },
  },
  breakpoints: {
    values: {
      lg: 1440,
      md: 800,
    },
  },
  typography: {
    h1: {
      fontFamily: 'Poppins',
      fontSize: '30px',
      lineHeight: '32px',
      color: 'white',
      fontWeight: 'bold',
      letterSpacing: '-0.11px',
    },
    h2: {
      fontFamily: 'Poppins',
      fontSize: '30px',
      lineHeight: '20px',
      color: '#009688',
      fontWeight: 'bold',
      letterSpacing: '-0.11px',
      marginTop: '32px',
      marginBottom: '32px',
    },
    h3: {
      fontFamily: 'Poppins',
      fontSize: '16px',
      lineHeight: '25px',
      color: '#4a4a4a',
      fontWeight: 'bold',
      letterSpacing: '-0.06px',
      marginBottom: '16px',
    },
    h4: {
      fontFamily: 'Poppins',
      fontSize: '14px',
      lineHeight: '16px',
      color: '#4a4a4a',
      fontWeight: 'bold',
      letterSpacing: '-0.05px',
    },
  },
  overrides: {
    MuiStepIcon: {
      root: {
        '&$completed': {
          color: 'white',
        },
        '&$active': {
          color: 'white',
        },
        color: '#B3DFDB',
      },
      text: {
        fontFamily: 'Poppins',
        fontSize: '16px',
        lineHeight: '16px',
        letterSpacing: '-0.06px',
        fontWeight: 'bold',
        color: '#009688',
        textTransform: 'uppercase',
        fill: '#009688',
      },
    },
    MuiStepLabel: {
      label: {
        '&$completed': {
          fontFamily: 'Poppins',
          fontSize: '16px',
          lineHeight: '20px',
          letterSpacing: '-0.06px',
          fontWeight: 'bold',
          color: 'white',
          textTransform: 'uppercase',
        },
        '&$active': {
          fontFamily: 'Poppins',
          fontSize: '16px',
          lineHeight: '20px',
          letterSpacing: '-0.06px',
          fontWeight: 'bold',
          color: 'white',
          textTransform: 'uppercase',
        },
        fontFamily: 'Poppins',
        fontSize: '16px',
        lineHeight: '20px',
        letterSpacing: '-0.06px',
        fontWeight: 'bold',
        color: '#B3DFDB',
        textTransform: 'uppercase',
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({}));

export default function App(props) {
  const styles = useStyles();

  return (
    <Router>
      <ApolloProvider client={client}>
        <ThemeProvider theme={theme}>
          <ScrollToTop />
          <TicketProvider>
            <div className={styles.root}>
              <CssBaseline />
              <TopBar />
              <AppContent />
            </div>
          </TicketProvider>
        </ThemeProvider>
      </ApolloProvider>
    </Router>
  );
}
