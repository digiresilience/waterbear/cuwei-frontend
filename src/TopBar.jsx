import React from 'react';
import { useLocation } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { Link as RouterLink } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import md5 from 'blueimp-md5';
import AppBar from '@material-ui/core/AppBar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import { CurrentUserQuery } from './queries/common/CurrentUserQuery.js';

export const TopBar = () => {
  const location = useLocation();
  const path = location.pathname;
  let section = '';
  if (path.startsWith('/tickets')) {
    section = 'Review';
  } else if (path.startsWith('/export')) {
    section = 'Export';
  } else if (path.startsWith('/admin')) {
    section = 'Admin';
  }

  const barHeight = 120;
  const useStyles = makeStyles((theme) => ({
    bar: {
      backgroundColor: theme.palette.primary.royalBlue,
      height: barHeight,
    },
    userName: {
      color: theme.palette.common.white,
    },
    menuItem: {
      fontSize: '110%',
      color: 'rgba(255, 255, 255, 0.5)',
      fontWeight: 'bold',
      marginRight: 50,
      textTransform: 'none',
    },
    menuItemSelected: {
      fontSize: '110%',
      color: theme.palette.common.white,
      fontWeight: 'bold',
      marginRight: 50,
      borderBottom: '3px solid white',
      borderRadius: 0,
      textTransform: 'none',
    },
  }));
  const styles = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [currentUser, setCurrentUser] = React.useState(null);

  const handleOpenMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  useQuery(CurrentUserQuery, {
    onCompleted: (data) => {
      if (data && data.currentUser) {
        setCurrentUser(data.currentUser);
      }
    },
    onError: (error) => {
      console.log(error);
    },
  });

  return (
    <AppBar position="sticky" className={styles.bar}>
      <Container maxWidth="md">
        <Grid
          container
          direction="row"
          alignItems="center"
          justify="space-between"
          style={{ height: barHeight }}
        >
          <Grid item>
            <a href="/#/">
              <Typography variant="h1">DISINFO</Typography>
            </a>
          </Grid>
          {currentUser && (
            <Grid item>
              <Grid container>
                {currentUser.userRole !== 'INVESTIGATOR' && (
                  <Grid item>
                    <Button
                      component={RouterLink}
                      className={
                        section === 'Review'
                          ? styles.menuItemSelected
                          : styles.menuItem
                      }
                      to={{
                        pathname: '/tickets',
                      }}
                    >
                      Review
                    </Button>
                  </Grid>
                )}
                <Grid item>
                  <Button
                    component={RouterLink}
                    className={
                      section === 'Export'
                        ? styles.menuItemSelected
                        : styles.menuItem
                    }
                    to={{
                      pathname: '/export',
                    }}
                  >
                    Export
                  </Button>
                </Grid>

                <Grid item>
                  <Button onClick={handleOpenMenu}>
                    <Avatar
                      src={
                        currentUser.email === 'loading'
                          ? ``
                          : `https://www.gravatar.com/avatar/${md5(
                              currentUser.email,
                            )}?d=404`
                      }
                      alt={currentUser.name}
                    >
                      {currentUser.name
                        ? currentUser.name.match(/\b\w/g).join('')
                        : ''}
                    </Avatar>
                  </Button>
                  <Menu
                    id="user-menu"
                    anchorEl={anchorEl}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                    open={Boolean(anchorEl)}
                    onClose={handleCloseMenu}
                    getContentAnchorEl={null}
                  >
                    {currentUser.userRole === 'ADMIN' && (
                      <MenuItem
                        onClick={handleCloseMenu}
                        component={RouterLink}
                        to="/admin/users"
                      >
                        Admin
                      </MenuItem>
                    )}
                    <MenuItem onClick={handleCloseMenu}>
                      <Link
                        href="https://disinfo.cloudflareaccess.com/cdn-cgi/access/logout"
                        color="inherit"
                      >
                        Sign out
                      </Link>
                    </MenuItem>
                  </Menu>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      </Container>
    </AppBar>
  );
};
