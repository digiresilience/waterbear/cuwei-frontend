import gql from 'graphql-tag';

/**
 * input is
 * { userName: "Abel Luck", userEmail: "abel@example.com" }
 */
export const CreateFirstUserMutation = gql`
mutation CreateFirstUser ($input: CreateFirstUserInput!) {
  createFirstUser(input: $input) {
    users {
      id
    }
  }
}`;
