import gql from 'graphql-tag';

export const UsersQuery = gql`
  query UsersQuery {
    users {
      nodes {
        id
        name
        userRole
        isActive
        email
        filterTags
        updatedAt
        createdBy
        createdAt
      }
    }
  }
`;
