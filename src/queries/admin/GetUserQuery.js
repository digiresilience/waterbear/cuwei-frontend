import gql from 'graphql-tag';

export const GetUserQuery = gql`
  query GetUserQuery($id: UUID!) {
    user(id: $id) {
      id
      name
      userRole
      isActive
      email
      filterTags
      updatedAt
      createdBy
      createdAt
    }
  }
`;
