import gql from 'graphql-tag';

export const UpdateUserMutation = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      clientMutationId
      user {
        id
        name
        userRole
        isActive
        email
        filterTags
        updatedAt
        createdBy
        createdAt
      }
    }
  }
`;
