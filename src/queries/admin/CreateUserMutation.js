import gql from 'graphql-tag';

export const CreateUserMutation = gql`
  mutation CreateUser($input: CreateUserInput!) {
    createUser(input: $input) {
      user {
        id
        name
        userRole
        isActive
        email
        filterTags
        updatedAt
        createdBy
        createdAt
      }
    }
  }
`;
