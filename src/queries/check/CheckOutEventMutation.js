import gql from 'graphql-tag';

export const CheckOutEventMutation = gql`
  mutation CheckOutEventMutation($clientMutationId: String!) {
    checkOutEvent(input: { clientMutationId: $clientMutationId }) {
      event {
        id
        isApproved
        isHeld
        eventNumber
        attributes {
          nodes {
            id
            field {
              name
              displayName
              id
              fieldType
            }
            tags {
              nodes {
                id
                name
                displayName
              }
            }
            isHeld
            isBinary
            value
            valueEdited
            metadata
          }
        }
      }
    }
  }
`;
