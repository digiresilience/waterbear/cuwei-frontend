import gql from 'graphql-tag';

export const CheckOutEventByIDMutation = gql`
  mutation CheckOutEventByIDMutation(
    $eventId: UUID!
    $clientMutationId: String!
  ) {
    checkOutEventById(
      input: { eventId: $eventId, clientMutationId: $clientMutationId }
    ) {
      event {
        id
        isApproved
        isHeld
        eventNumber
        tags {
          nodes {
            id
            name
            displayName
          }
        }
        attributes {
          nodes {
            id
            field {
              name
              displayName
              id
              fieldType
            }
            isHeld
            isBinary
            value
            valueEdited
            metadata
          }
        }
      }
    }
  }
`;
