import gql from 'graphql-tag';

export const CheckInEventMutation = gql`
mutation CheckInEventMutation($input: CheckInEventInput!) {
  checkInEvent(input: $input) {
    event {
      id
      createdAt
      updatedAt
    }
  }
}`;
