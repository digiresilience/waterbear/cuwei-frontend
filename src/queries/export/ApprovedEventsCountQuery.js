import gql from 'graphql-tag';

export const ApprovedEventsCountQuery = gql`
  query ApprovedEventsCountQuery {
    approvedEventsCount
  }
`;
