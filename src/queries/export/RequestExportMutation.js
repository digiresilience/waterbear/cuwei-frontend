import gql from 'graphql-tag';

export const RequestExportMutation = gql`
  mutation RequestEventMutation($clientMutationId: String!) {
    requestExport(input: { clientMutationId: $clientMutationId }) {
      exportRequest {
        exportRequestId
      }
    }
  }
`;
