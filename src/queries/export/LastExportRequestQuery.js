import gql from 'graphql-tag';

export const LastExportRequestQuery = gql`
  query LastExportRequestQuery {
    currentUser {
      id
      exportRequestsByRequestingUserId(orderBy: REQUESTED_AT_DESC, first: 1) {
        nodes {
          id
          requestedAt
        }
      }
    }
  }
`;
