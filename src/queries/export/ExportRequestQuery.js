import gql from 'graphql-tag';

export const ExportRequestQuery = gql`
  query ExportRequestQuery($exportRequestID: UUID!) {
    exportRequestByExportRequestId(exportRequestId: $exportRequestID) {
      id
      status
      downloadUrl
      requestedAt
      completedAt
    }
  }
`;
