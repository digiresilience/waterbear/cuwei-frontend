import gql from 'graphql-tag';

export const RecentApprovedEventsCountQuery = gql`
  query RecentApprovedEventsCountQuery {
    recentApprovedEventsCount
  }
`;
