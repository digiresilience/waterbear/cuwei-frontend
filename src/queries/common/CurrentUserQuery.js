import gql from 'graphql-tag';

export const CurrentUserQuery = gql`
  query CurrentUserQuery {
    currentUser {
      id
      name
      userRole
      isActive
      email
      filterTags
      updatedAt
      createdBy
      createdAt
    }
  }
`;
