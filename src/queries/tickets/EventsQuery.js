import gql from 'graphql-tag';

export const EventsQuery = gql`
  query EventsQuery {
    events(first: 100) {
      nodes {
        id
        isApproved
        isHeld
        isArchived
        eventNumber
        reportedAt
        reviews(first: 1) {
          nodes {
            reason
          }
        }
        attributes {
          nodes {
            id
            value
            field {
              name
            }
          }
        }
      }
    }
  }
`;
