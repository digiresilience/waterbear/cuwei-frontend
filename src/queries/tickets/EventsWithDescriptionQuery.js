import gql from 'graphql-tag';

export const EventsWithDescriptionQuery = gql`
  query EventsWithDescriptionQuery {
    eventsWithDescription(first: 100) {
      nodes {
        id
        eventNumber
        isHeld
        isArchived
        isApproved
        reportedAt
        description
        descriptionEdited
        reason
      }
    }
  }
`;
