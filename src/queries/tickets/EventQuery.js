import gql from 'graphql-tag';

export const EventQuery = gql`
  query EventQuery($id: UUID!) {
    event(id: $id) {
      id
      isHeld
      isApproved
      isArchived
      eventNumber
      reportedAt
      reviews(first: 1) {
        nodes {
          reason
        }
      }
      tags {
        nodes {
          id
          name
          displayName
        }
      }
      attributes {
        nodes {
          id
          field {
            name
            id
            fieldType
            fieldOptions {
              nodes {
                id
                name
                displayName
              }
            }
          }
          isHeld
          isBinary
          value
          valueEdited
          metadata
        }
      }
    }
  }
`;
