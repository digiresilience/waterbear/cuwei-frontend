import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { Route, Redirect } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Tickets } from './components/tickets/Tickets.jsx';
import { Export } from './components/export/Export.jsx';
import { Admin } from './components/admin/Admin.jsx';
import { AlertProvider } from './AlertProvider.jsx';
import { CurrentUserQuery } from './queries/common/CurrentUserQuery.js';

export const AppContent = () => {
  const [currentUser, setCurrentUser] = React.useState(undefined);
  const location = useLocation();
  const path = location.pathname;

  useQuery(CurrentUserQuery, {
    onCompleted: (data) => {
      if (data && data.currentUser) {
        setCurrentUser(data.currentUser);
      }
    },
    onError: (error) => {
      console.log(error);
    },
  });

  if (!currentUser) {
    return <CircularProgress />;
  }

  if (currentUser.userRole === 'INVESTIGATOR' && !path.startsWith('/export')) {
    return <Redirect to="/export" />;
  }

  return (
    <Container maxWidth="md" component="main">
      <AlertProvider>
        <Route exact path="/">
          <Redirect to="/tickets" />
        </Route>
        <Route path="/tickets">
          <Tickets />
        </Route>
        <Route path="/export">
          <Export />
        </Route>
        <Route path="/admin">
          <Admin />
        </Route>
      </AlertProvider>
    </Container>
  );
};
