import React, { createContext, useState } from 'react';
import { Collapse } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';

export const AlertContext = createContext({});

export const AlertProvider = ({ children }) => {
  const [alertOpen, setAlertOpen] = useState(false);
  const [alertSeverity, setAlertSeverity] = useState('error');
  const [alertTitle, setAlertTitle] = useState('');
  const [alertText, setAlertText] = useState('');

  const displayAlert = (severity, title, text) => {
    setAlertSeverity(severity);
    setAlertTitle(title);
    setAlertText(text);
    setAlertOpen(true);
    const timer = setTimeout(() => {
      setAlertOpen(false);
      return () => clearTimeout(timer);
    }, 10000);
  };

  return (
    <AlertContext.Provider
      value={{
        displayAlert,
      }}
    >
      <>
        <Collapse in={alertOpen}>
          <Alert style={{ marginTop: 30 }} severity={alertSeverity}>
            <AlertTitle>{alertTitle}</AlertTitle>
            {alertText}
          </Alert>
        </Collapse>
        {children}
      </>
    </AlertContext.Provider>
  );
};
