import React from 'react';
import { Route } from 'react-router-dom';
import { Download } from './Download.jsx';

export const Export = () => {
  return (
    <Route path="/export">
      <Download />
    </Route>
  );
};
