import React, { useContext, useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import { AlertContext } from '../../AlertProvider.jsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { ApprovedEventsCountQuery } from '../../queries/export/ApprovedEventsCountQuery.js';
import { RecentApprovedEventsCountQuery } from '../../queries/export/RecentApprovedEventsCountQuery.js';
import { RequestExportMutation } from '../../queries/export/RequestExportMutation.js';
import { ExportRequestQuery } from '../../queries/export/ExportRequestQuery.js';
import { LastExportRequestQuery } from '../../queries/export/LastExportRequestQuery.js';
import { v4 as uuidv4 } from 'uuid';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    paddingTop: theme.spacing(20),
    marginBottom: theme.spacing(2),
  },
  lastDownload: {
    fontWeight: 'bold',
    marginBottom: 20,
  },
  downloadContents: { marginTop: 20 },
}));

export const Download = () => {
  const styles = useStyles();
  const { displayAlert } = useContext(AlertContext);
  const [exportInProgress, setExportInProgress] = useState(false);
  const [downloadURL, setDownloadURL] = useState(null);
  const [exportRequestID, setExportRequestID] = useState(uuidv4().toString());
  const [lastDownloadDate, setLastDownloadDate] = useState(null);
  const [totalTicketCount, setTotalTicketCount] = useState(0);
  const [recentTicketCount, setRecentTicketCount] = useState(0);
  const [requestExportMutation] = useMutation(RequestExportMutation, {
    variables: {
      clientMutationId: 'requestExportMutation',
    },
    onCompleted: (data) => {
      const exportRequest = data.requestExport.exportRequest;
      setExportRequestID(exportRequest.exportRequestId);
    },
    onError: (error) => {
      displayAlert('error', 'Error', error.toString());
    },
  });

  const { loading: loadingAll } = useQuery(ApprovedEventsCountQuery, {
    onCompleted: (data) => {
      if (data && data.approvedEventsCount) {
        setTotalTicketCount(data.approvedEventsCount);
      }
    },
    onError: (error) => {
      displayAlert('error', 'Error', error.toString());
    },
  });

  const { loading: loadingRecent } = useQuery(RecentApprovedEventsCountQuery, {
    onCompleted: (data) => {
      if (data && data.recentApprovedEventsCount) {
        setRecentTicketCount(data.recentApprovedEventsCount);
      }
    },
    onError: (error) => {
      displayAlert('error', 'Error', error.toString());
    },
  });

  const { loading: loadingLast } = useQuery(LastExportRequestQuery, {
    onCompleted: (data) => {
      const requests = data.currentUser.exportRequestsByRequestingUserId.nodes;
      if (requests.length > 0) {
        const request = requests[0];
        setLastDownloadDate(new Date(request.requestedAt));
      }
    },
    onError: (error) => {
      displayAlert('error', 'Error', error.toString());
    },
  });

  useQuery(ExportRequestQuery, {
    variables: { exportRequestID },
    cachePolicy: 'network',
    pollInterval: 2000,
    partialRefetch: true,
    notifyOnNetworkStatusChange: true,
    onCompleted: (data) => {
      if (data && data.exportRequestByExportRequestId) {
        const exportRequest = data.exportRequestByExportRequestId;
        if (exportRequest.status === 'COMPLETED') {
          setExportInProgress(false);
          setDownloadURL(exportRequest.downloadUrl);
        } else if (exportRequest.status === 'ERROR') {
          setExportInProgress(false);
          displayAlert('error', 'Error', 'Export failed');
        }
      }
    },
    onError: (error) => {
      displayAlert('error', 'Error', error.toString());
    },
  });

  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      className={styles.root}
    >
      <Typography variant="h2">Download approved tickets.</Typography>
      <Typography variant="h3">
        {totalTicketCount} total tickets, {recentTicketCount} new since your
        last download.
      </Typography>
      <Typography variant="body1" className={styles.lastDownload}>
        Your last download was
        {lastDownloadDate
          ? ' ' + lastDownloadDate.toLocaleString()
          : ' - never'}
        .
      </Typography>

      <Box>
        <Button
          variant="contained"
          onClick={() => {
            setExportInProgress(true);
            requestExportMutation();
          }}
        >
          Download All
        </Button>
        <Button
          onClick={() => {
            setExportInProgress(true);
            requestExportMutation();
          }}
        >
          Download New
        </Button>
      </Box>
      <Typography variant="body2" className={styles.downloadContents}>
        Download is a .zip file containing a .csv file containing ticket info
        and external files for supporting media.
      </Typography>
      {exportInProgress || loadingAll || loadingRecent || loadingLast ? (
        <CircularProgress />
      ) : (
        ''
      )}
      {downloadURL ? (
        <Box style={{ padding: '20px', backgroundColor: '#fbfbfb' }}>
          <Typography variant="h3">Your export is ready</Typography>
          <Typography variant="body1">
            <Link href={downloadURL} target="_blank">
              Click here
            </Link>{' '}
            to download it.
          </Typography>
        </Box>
      ) : (
        ''
      )}
    </Grid>
  );
};
