import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { BreadcrumbNavigator } from './BreadcrumbNavigator';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import Button from '@material-ui/core/Button';
import { Link as RouterLink } from 'react-router-dom';
import { Text } from './Text.jsx';
import { Media } from './Media.jsx';
import { Links } from './Links.jsx';
import { Comments } from './Comments.jsx';
import { Tags } from './Tags.jsx';
import { TicketContext } from '../../TicketProvider.jsx';
import { makeStyles } from '@material-ui/core/styles';
import { EventQuery } from '../../queries/tickets/EventQuery.js';
import { HoldRejectButton } from './HoldRejectButton.jsx';

const useStyles = makeStyles((theme) => ({
  rightToggle: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(2),
    background:
      'linear-gradient(to right, rgba(255,255,255,1) 90%, rgba(255,255,255,1) 90%, rgba(250,250,250,1) 90%)',
  },
  embeddedToggle: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(2),
    background: theme.palette.common.white,
  },
  footer: {
    padding: theme.spacing(1),
  },
  holdButton: {
    backgroundColor: theme.palette.primary.royalBlue,
    color: theme.palette.common.white,
    margin: theme.spacing(3, 1, 1, 1),
  },
  approveButton: {
    margin: theme.spacing(3, 3, 3, 9),
  },
}));

export const Review = () => {
  const {
    setTicket,
    ticketNumber,
    description,
    descriptionDispatch,
    attachments,
    attachmentsDispatch,
    links,
    linksDispatch,
    comments,
    commentsDispatch,
    date,
    dateDispatch,
    geography,
    geographyDispatch,
    reason,
    reasonDispatch,
    medium,
    mediumDispatch,
    additionalInfo,
    additionalInfoDispatch,
    sensitivity,
    sensitivityDispatch,
    ticketID,
  } = useContext(TicketContext);
  const styles = useStyles();
  const { id } = useParams();
  const { loading, error } = useQuery(EventQuery, {
    variables: { id },
    onCompleted: (data) => {
      const event = data.event;
      setTicket(event);
    },
  });

  if (loading) {
    return <CircularProgress />;
  }

  if (error) {
    console.log(error);
    return <div>Error</div>;
  }

  return (
    <>
      <Container maxWidth="md">
        <BreadcrumbNavigator page="review" />
        <Typography
          variant="h2"
          style={{ marginTop: 24 }}
        >{`Review Ticket #${ticketNumber}`}</Typography>
        <Paper className={styles.rightToggle} variant="outlined">
          <Text
            field={description}
            reducer={descriptionDispatch}
            editable={true}
          />
        </Paper>
        <Paper className={styles.embeddedToggle} variant="outlined">
          <Media
            field={attachments}
            reducer={attachmentsDispatch}
            editable={true}
          />
        </Paper>
        <Paper className={styles.rightToggle} variant="outlined">
          <Links field={links} reducer={linksDispatch} editable={true} />
        </Paper>
        <Paper className={styles.rightToggle} variant="outlined">
          <Comments
            field={comments}
            reducer={commentsDispatch}
            editable={true}
          />
        </Paper>
        <Paper className={styles.rightToggle} variant="outlined">
          <Text
            field={date}
            reducer={dateDispatch}
            editable={true}
            holdOnly={true}
          />
        </Paper>
        <Paper className={styles.rightToggle} variant="outlined">
          <Text
            field={geography}
            reducer={geographyDispatch}
            editable={true}
            holdOnly={true}
          />
        </Paper>
        <Paper className={styles.rightToggle} variant="outlined">
          <Text
            field={reason}
            reducer={reasonDispatch}
            editable={true}
            holdOnly={true}
          />
        </Paper>
        <Paper className={styles.rightToggle} variant="outlined">
          <Text
            field={medium}
            reducer={mediumDispatch}
            editable={true}
            holdOnly={true}
          />
        </Paper>
        <Paper className={styles.rightToggle} variant="outlined">
          <Text
            field={additionalInfo}
            reducer={additionalInfoDispatch}
            editable={true}
          />
        </Paper>
        <Paper className={styles.rightToggle} variant="outlined">
          <Text
            field={sensitivity}
            reducer={sensitivityDispatch}
            editable={true}
            holdOnly={true}
          />
        </Paper>
        <Paper className={styles.embeddedToggle} variant="outlined">
          <Tags editable={true} />
        </Paper>
        <Grid
          container
          direction="row"
          className={styles.footer}
          justify="center"
          alignItems="flex-start"
        >
          <Grid item xs={9} style={{ textAlign: 'left' }}>
            <HoldRejectButton action="reject" stage="review" />
            <HoldRejectButton action="hold" stage="review" />
          </Grid>
          <Grid item xs={3}>
            <Button
              color="primary"
              className={styles.approveButton}
              component={RouterLink}
              to={{
                pathname: `/tickets/${ticketID}/approve`,
              }}
            >
              Approve
              <ArrowForwardIcon />
            </Button>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};
