import React, { useContext } from 'react';
import { withRouter } from 'react-router';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import StepLabel from '@material-ui/core/StepLabel';
import { makeStyles } from '@material-ui/core/styles';
import { TicketContext } from '../../TicketProvider.jsx';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const useStyles = makeStyles(theme => ({
  stepsHeader: {
    color: theme.palette.primary.teal,
    backgroundColor: theme.palette.primary.lightGray,
    padding: theme.spacing(4),
  },
  stepper: { backgroundColor: theme.palette.primary.lightGray },
}));

export const Header = withRouter(props => {
  const styles = useStyles();
  const { eventNumber } = useContext(TicketContext);

  return (
    <Box className={styles.stepsHeader}>
      <Container maxWidth="md">
        <Stepper
          className={styles.stepper}
          activeStep={props.activeStep}
          alternativeLabel
        >
          <Step key={'Review'}>
            <StepLabel>Review</StepLabel>
          </Step>
          <Step key={'Approve'}>
            <StepLabel>Approve</StepLabel>
          </Step>
        </Stepper>
      </Container>
    </Box>
  );
});
