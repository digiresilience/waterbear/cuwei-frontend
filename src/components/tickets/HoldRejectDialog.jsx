import React, { useContext, useState } from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core/';
import TextField from '@material-ui/core/TextField';
import { useHistory } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { TicketContext } from '../../TicketProvider.jsx';
import { useMutation } from '@apollo/react-hooks';
import { CheckInEventMutation } from '../../queries/check/CheckInEventMutation.js';
import { AlertContext } from '../../AlertProvider.jsx';

const useStyles = makeStyles(theme => ({
  title: {
    color: theme.palette.primary.darkGray,
    margin: theme.spacing(3),
  },
  cancelButton: {
    color: theme.palette.primary.royalBlue,
    margin: theme.spacing(3, 1, 1, 1),
  },
  continueButton: {
    backgroundColor: theme.palette.primary.royalBlue,
    color: theme.palette.common.white,
    margin: theme.spacing(3, 1, 1, 1),
  },
}));

export const HoldRejectDialog = ({ open, action, onHide }) => {
  const styles = useStyles();
  const history = useHistory();
  const { stateToGraphQL } = useContext(TicketContext);
  const [reason, setReason] = useState('');
  const { displayAlert } = useContext(AlertContext);
  const [checkInEventMutation] = useMutation(CheckInEventMutation, {
    onCompleted: data => {
      onHide();
      history.push(`/tickets`);
    },
    onError: error => {
      displayAlert('error', 'Save Error', error.toString());
    },
  });

  return (
    <Dialog open={open}>
      <Typography
        variant="h3"
        className={styles.title}
      >{`Why are you ${action}ing this ticket?`}</Typography>
      <DialogContent>
        <TextField
          multiline
          rows={2}
          fullWidth
          value={reason}
          onChange={e => setReason(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Grid container justify="space-around">
          <Grid item>
            <Button
              disableElevation={true}
              className={styles.cancelButton}
              onClick={() => {
                onHide();
              }}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              disableElevation={true}
              className={styles.continueButton}
              onClick={() => {
                checkInEventMutation({
                  variables: {
                    input: stateToGraphQL(
                      action === 'reject' ? 'archive' : 'hold',
                      reason,
                    ),
                  },
                  onComplete: data => {
                    onHide();
                  },
                });
              }}
            >
              {action}
            </Button>
          </Grid>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};
