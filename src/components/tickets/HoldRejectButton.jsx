import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { HoldRejectDialog } from './HoldRejectDialog.jsx';

const useStyles = makeStyles(theme => ({
  reviewButtons: {
    backgroundColor: theme.palette.primary.royalBlue,
    color: theme.palette.common.white,
    margin: theme.spacing(3, 1, 1, 1),
    '&:hover': {
      backgroundColor: theme.palette.primary.darkBlue,
    },
  },
  approveButtons: {
    backgroundColor: theme.palette.primary.royalBlue,
    color: theme.palette.common.white,
    margin: theme.spacing(3),
    '&:hover': {
      backgroundColor: theme.palette.primary.darkBlue,
    },
  },
}));

export const HoldRejectButton = ({ action, stage }) => {
  const styles = useStyles();
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <HoldRejectDialog
        action={action}
        open={dialogOpen}
        onHide={() => setDialogOpen(false)}
      />
      <Button
        variant="contained"
        disableElevation={true}
        className={
          stage === 'approve' ? styles.approveButtons : styles.reviewButtons
        }
        onClick={() => {
          setDialogOpen(true);
        }}
      >
        {action}
      </Button>
    </>
  );
};
