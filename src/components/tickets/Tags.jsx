import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { TicketContext } from '../../TicketProvider.jsx';
import { EditableTagSet } from './EditableTagSet.jsx';
import { DisplayTagSet } from './DisplayTagSet.jsx';

const useStyles = makeStyles((theme) => ({
  held: {
    opacity: '0.25',
  },
}));

export const Tags = ({ editable }) => {
  const styles = useStyles();

  const {
    tactics,
    tacticsDispatch,
    platforms,
    platformsDispatch,
    parties,
    partiesDispatch,
    candidates,
    candidatesDispatch,
    actors,
    actorsDispatch,
    communities,
    communitiesDispatch,
    customTags,
    customTagsDispatch,
  } = useContext(TicketContext);

  const tagTypes = [
    [tactics, tacticsDispatch, '#2962FF'],
    [platforms, platformsDispatch, '#880E4F'],
    [parties, partiesDispatch, '#5E35B1'],
    [candidates, candidatesDispatch, '#C51162'],
    [actors, actorsDispatch, '#33691E'],
    [communities, communitiesDispatch, '#BF360C'],
    [customTags, customTagsDispatch, '#ffaa33'],
  ];

  const enabledTags = tagTypes
    .map((tagType) => tagType[0].values.filter((val) => val.valueEdited))
    .flat();

  return (
    <>
      <Typography variant="h3">Tags</Typography>
      {!editable && enabledTags.length === 0 ? (
        <Typography variant="body1" className={styles.held}>
          (No value)
        </Typography>
      ) : undefined}

      {tagTypes.map(([field, reducer, color]) => {
        return editable ? (
          <EditableTagSet
            key={field.displayName}
            field={field}
            reducer={reducer}
            color={color}
          />
        ) : (
          <DisplayTagSet
            key={field.displayName}
            field={field}
            reducer={reducer}
            color={color}
          />
        );
      })}
    </>
  );
};
