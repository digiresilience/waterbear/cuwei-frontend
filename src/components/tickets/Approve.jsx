import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import { Text } from './Text.jsx';
import { Media } from './Media.jsx';
import { Links } from './Links.jsx';
import { BreadcrumbNavigator } from './BreadcrumbNavigator';
import { Link as RouterLink } from 'react-router-dom';
import { useMutation } from '@apollo/react-hooks';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { TicketContext } from '../../TicketProvider.jsx';
import { Tags } from './Tags.jsx';
import { Comments } from './Comments.jsx';
import { CheckInEventMutation } from '../../queries/check/CheckInEventMutation.js';
import { HoldRejectButton } from './HoldRejectButton.jsx';

const useStyles = makeStyles((theme) => ({
  content: {
    padding: theme.spacing(2),
  },
  item: {
    marginBottom: theme.spacing(4),
  },
  oversized: {
    fontSize: '300%',
    color: theme.palette.primary.darkGray,
    marginBottom: 20,
  },
  resolution: {
    textAlign: 'center',
    paddingTop: '30px',
  },
  holdApproveButton: {
    backgroundColor: theme.palette.primary.royalBlue,
    color: theme.palette.common.white,
    margin: '24px',
    '&:hover': {
      backgroundColor: theme.palette.primary.darkBlue,
    },
  },
  verify: {
    fontFamily: 'Roboto',
    fontSize: '18px',
    lineHeight: '21px',
    color: '#4A4A4A',
    textAlign: 'left',
  },
}));

export const Approve = () => {
  const {
    ticketID,
    ticketNumber,
    description,
    attachments,
    links,
    comments,
    date,
    geography,
    reason,
    medium,
    additionalInfo,
    sensitivity,
    tags,
    stateToGraphQL,
  } = useContext(TicketContext);
  const styles = useStyles();
  const history = useHistory();
  const [verified, setVerified] = useState(false);
  const handleCheckboxChange = (e) => {
    setVerified(e.target.checked);
  };
  const [checkInEventMutation] = useMutation(CheckInEventMutation, {
    onCompleted: (data) => {
      history.push(`/tickets`);
    },
    onError: (error) => {
      console.log(error);
    },
  });

  return (
    <>
      <Container maxWidth="md">
        <BreadcrumbNavigator page="approve" />
        <Typography variant="h2" style={{ marginTop: 24 }} component="h2">
          {`Approve Ticket #${ticketNumber}`}
        </Typography>
        <Paper variant="outlined" className={styles.content}>
          <Box className={styles.item}>
            <Text field={description} editable={false} />
          </Box>
          <Box className={styles.item}>
            <Media field={attachments} editable={false} />
          </Box>
          <Box className={styles.item}>
            <Links field={links} editable={false} />
          </Box>
          <Box>
            <Comments field={comments} editable={false} />
          </Box>
          <Box className={styles.item}>
            <Text field={date} editable={false} />
          </Box>
          <Box className={styles.item}>
            <Text field={geography} editable={false} />
          </Box>
          <Box className={styles.item}>
            <Text field={reason} editable={false} />
          </Box>
          <Box className={styles.item}>
            <Text field={medium} editable={false} />
          </Box>
          <Box className={styles.item}>
            <Text field={additionalInfo} editable={false} />
          </Box>
          <Box className={styles.item}>
            <Text field={sensitivity} editable={false} />
          </Box>
          <Box className={styles.item}>
            <Tags field={tags} editable={false} />
          </Box>
          <Container maxWidth="sm" className={styles.resolution}>
            <Typography variant="h1" className={styles.oversized}>
              Everything look right?
            </Typography>
            <Grid container direction="row" alignItems="center">
              <Grid item xs={2}>
                <Checkbox
                  checked={verified}
                  className="large-checkbox"
                  color="primary"
                  onChange={handleCheckboxChange}
                />
              </Grid>
              <Grid item xs={8}>
                <Typography className={styles.verify}>
                  I verify that while editing, the integrity of the original
                  case has been maintained and information has only been
                  redacted for the safety of the submitter.
                </Typography>
              </Grid>
            </Grid>
            <Box>
              <HoldRejectButton action="reject" stage="approve" />
              <HoldRejectButton action="hold" stage="approve" />
              <Button
                className={styles.holdApproveButton}
                variant="contained"
                disabled={!verified}
                onClick={() => {
                  checkInEventMutation({
                    variables: {
                      input: stateToGraphQL('approve', ''),
                    },
                  });
                }}
              >
                Approve
              </Button>
            </Box>
            <Button
              component={RouterLink}
              to={{
                pathname: `/tickets/${ticketID}/review`,
              }}
            >
              Back to Review
            </Button>
          </Container>
        </Paper>
      </Container>
    </>
  );
};
