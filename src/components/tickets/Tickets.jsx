import React from 'react';
import { Route } from 'react-router-dom';
import { Start } from './Start.jsx';
import { Review } from './Review.jsx';
import { Approve } from './Approve.jsx';

export const Tickets = props => {
  return (
    <>
      <Route path="/tickets/:id/review">
        <Review />
      </Route>
      <Route path="/tickets/:id/approve">
        <Approve />
      </Route>
      <Route exact path="/tickets">
        <Start />
      </Route>
    </>
  );
};
