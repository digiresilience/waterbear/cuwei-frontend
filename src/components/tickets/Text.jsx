import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  switchContainer: {
    position: 'relative',
    right: '-8px',
  },
  primaryButton: {
    color: theme.palette.primary.teal,
  },
  displayHeader: {
    marginBottom: '4px',
  },
  displayText: {},
  icons: {
    marginTop: theme.spacing(-1),
    marginRight: theme.spacing(2),
  },
  held: {
    opacity: '0.25',
  },
  originalText: {
    fontFamily: 'Roboto',
    fontSize: '14px',
    lineHeight: '18px',
    color: '#4A4A4A',
    fontStyle: 'italic',
    marginTop: theme.spacing(2),
  },
}));

const EditableText = ({ field, reducer, holdOnly }) => {
  const styles = useStyles();
  const [edited, setEdited] = useState(false);
  const [showOriginal, setShowOriginal] = useState(false);

  const handleTextChange = (id, value) => {
    setEdited(true);
    reducer({
      type: 'updateValue',
      payload: { id, value },
    });
  };

  const handleSwitchChange = (id) => {
    reducer({ type: 'toggleHeld', payload: { id } });
  };

  const handleShowOriginalClick = () => {
    setShowOriginal(!showOriginal);
  };

  const handleRevertClick = (allIDs) => {
    setEdited(false);
    setShowOriginal(false);
    allIDs.forEach((id) => {
      reducer({ type: 'revertValue', payload: { id } });
    });
  };

  if (holdOnly) {
    return (
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="flex-start"
      >
        <Grid item xs={9}>
          <Typography variant="h3">{field.displayName}</Typography>
        </Grid>
        {field.values.map((val) => {
          return (
            <>
              <Grid item xs={9}>
                <Typography variant="body1">{val.valueEdited}</Typography>
              </Grid>
              <Grid item className={styles.switchContainer}>
                <Tooltip
                  placement="right"
                  title={val.held ? 'Not sent to database' : 'Sent to database'}
                >
                  <Switch
                    color="primary"
                    checked={!val.held}
                    onChange={() => handleSwitchChange(val.id)}
                  />
                </Tooltip>
              </Grid>
            </>
          );
        })}
      </Grid>
    );
  }

  return (
    <Box key={field.displayName}>
      <Grid
        className="temp"
        container
        direction="row"
        justify="space-between"
        alignItems="flex-start"
      >
        <Grid item xs={7}>
          <Typography variant="h3">{field.displayName}</Typography>
        </Grid>
        <Grid item xs={3} className={styles.icons}>
          {edited && showOriginal ? (
            <>
              <Button
                onClick={() =>
                  handleRevertClick(field.values.map((val) => val.id))
                }
                classes={{ text: styles.primaryButton }}
              >
                Revert
              </Button>
              <Button onClick={handleShowOriginalClick}>Done</Button>
            </>
          ) : (
            ''
          )}
          {edited && !showOriginal ? (
            <Button
              onClick={handleShowOriginalClick}
              classes={{ text: styles.primaryButton }}
            >
              Show Original
            </Button>
          ) : (
            ''
          )}
        </Grid>
        {field.values.map((val) => (
          <>
            <Grid item xs={9}>
              <TextField
                className="review-text"
                fullWidth
                multiline
                rows={3}
                value={val.valueEdited}
                onChange={(event) => {
                  handleTextChange(val.id, event.target.value);
                }}
              />
            </Grid>
            <Grid item xs={9}>
              {showOriginal ? (
                <Typography className={styles.originalText}>
                  {val.value}
                </Typography>
              ) : (
                ''
              )}
            </Grid>
            <Grid item className={styles.switchContainer}>
              <Tooltip
                placement="right"
                title={val.held ? 'Not sent to database' : 'Sent to database'}
              >
                <Switch
                  color="primary"
                  checked={!val.held}
                  onChange={() => handleSwitchChange(val.id)}
                />
              </Tooltip>
            </Grid>
          </>
        ))}
      </Grid>
    </Box>
  );
};

const DisplayText = ({ field }) => {
  const styles = useStyles();

  return (
    <Box key={field.displayName}>
      <Typography variant="h3" className={styles.displayHeader}>
        {field.displayName}
      </Typography>
      {field.values.map((val) => (
        <Typography
          variant="body1"
          key={val.id}
          className={
            val.held || !val.valueEdited ? styles.held : styles.displayText
          }
        >
          {val.valueEdited ? val.valueEdited : '(No value)'}
        </Typography>
      ))}
    </Box>
  );
};

export const Text = ({ field, reducer, editable, holdOnly }) => {
  return editable ? (
    <EditableText
      key={field.displayName}
      field={field}
      reducer={reducer}
      holdOnly={holdOnly}
    />
  ) : (
    <DisplayText key={field.displayName} field={field} />
  );
};
