import React from 'react';
import { useParams } from 'react-router-dom';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import { Link as RouterLink } from 'react-router-dom';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  breadcrumbs: {
    marginTop: 30,
    color: theme.palette.primary.mediumGray,
  },
  home: { marginTop: 6, color: theme.palette.primary.mediumGray },
  breadcrumb: {
    fontWeight: 'bold',
    textDecoration: 'none',
    color: theme.palette.primary.mediumGray,
  },
  currentBreadcrumb: {
    fontWeight: 'bold',
    textDecoration: 'none',
    color: theme.palette.primary.teal,
  },
}));

export const BreadcrumbNavigator = ({ page }) => {
  const styles = useStyles();
  const { id } = useParams();

  return (
    <Breadcrumbs
      separator={
        <NavigateNextIcon
          style={{ marginLeft: 8, marginRight: 8 }}
          fontSize="small"
        />
      }
      aria-label="breadcrumb"
      className={styles.breadcrumbs}
    >
      <RouterLink
        to={{
          pathname: `/tickets`,
        }}
      >
        <HomeIcon className={styles.home} fontSize="small" />
      </RouterLink>
      <RouterLink
        className={
          page === 'review' ? styles.currentBreadcrumb : styles.breadcrumb
        }
        to={{
          pathname: `/tickets/${id}/review`,
        }}
      >
        Review
      </RouterLink>
      <RouterLink
        className={
          page === 'approve' ? styles.currentBreadcrumb : styles.breadcrumb
        }
        to={{
          pathname: `/tickets/${id}/approve`,
        }}
      >
        Approve
      </RouterLink>
    </Breadcrumbs>
  );
};
