import React from 'react';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  linksContainer: {
    marginBottom: '18px',
  },
  switchContainer: {
    position: 'relative',
    right: '-8px',
  },
  held: {
    opacity: 0.25,
    color: theme.palette.primary.darkGray,
  },
}));

const EditLink = ({ link, index, handleChange, handleSwitchChange }) => {
  const styles = useStyles();

  return (
    <Grid item key={index}>
      <Grid container direction="row" justify="space-between">
        <Grid item xs={9}>
          <TextField
            fullWidth
            value={link.valueEdited}
            onChange={e => {
              handleChange(link.id, e.target.value);
            }}
          />
        </Grid>
        <Grid item className={styles.switchContainer}>
          <Tooltip
            placement="bottom"
            title={link.held ? 'Not sent to database' : 'Sent to database'}
          >
            <Switch
              color="primary"
              checked={!link.held}
              onChange={() => handleSwitchChange(link.id)}
            />
          </Tooltip>
        </Grid>
      </Grid>
    </Grid>
  );
};

const EditableLinks = ({ field, reducer }) => {
  const links = field.values;

  const handleChange = (id, value) => {
    reducer({
      type: 'updateValue',
      payload: { id, value },
    });
  };

  const handleSwitchChange = id => {
    reducer({ type: 'toggleHeld', payload: { id } });
  };

  return (
    <Grid container direction="column">
      {links.map((link, i) => (
        <EditLink
          key={i}
          link={link}
          index={i}
          handleChange={handleChange}
          handleSwitchChange={handleSwitchChange}
        />
      ))}
    </Grid>
  );
};

const DisplayLinks = ({ field }) => {
  const links = field.values;
  const styles = useStyles();

  return (
    <>
      {links.length === 0 ? (
        <Typography variant="body1" className={styles.held}>
          (No value)
        </Typography>
      ) : (
        undefined
      )}
      {links.map((link, i) => (
        <Typography key={i} variant="body1">
          <Link href={link.valueEdited}>
            <Box element="span" className={link.held ? styles.held : undefined}>
              {link.valueEdited}
            </Box>
          </Link>
        </Typography>
      ))}
    </>
  );
};

export const Links = ({ field, reducer, editable }) => {
  const styles = useStyles();
  return (
    <>
      <Typography variant="h3">{field.displayName}</Typography>
      <Grid container direction="column" className={styles.linksContainer}>
        {editable ? (
          <EditableLinks field={field} reducer={reducer} />
        ) : (
          <DisplayLinks field={field} />
        )}
      </Grid>
    </>
  );
};
