import React, { useState, useEffect, useContext } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { AlertContext } from '../../AlertProvider.jsx';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { EventsWithDescriptionQuery } from '../../queries/tickets/EventsWithDescriptionQuery';
import { CheckOutEventMutation } from '../../queries/check/CheckOutEventMutation.js';
import { CheckOutEventByIDMutation } from '../../queries/check/CheckOutEventByIDMutation.js';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  lastDownload: {
    fontWeight: 'bold',
  },
  summary: {
    width: '250px',
    textAlign: 'center',
    float: 'right',
  },
  summaryNumber: {
    color: '#009688',
    fontFamily: 'Poppins',
    fontWeight: 'bold',
    fontSize: '36px',
    lineHeight: '47px',
    letterSpacing: '-0.09px',
  },
  summaryTitle: {
    color: '#4A4A4A',
    fontFamily: 'Poppins',
    fontWeight: 'bold',
    fontSize: '18px',
    lineHeight: '27px',
    letterSpacing: '-0.04px',
  },
  preview: {
    maxWidth: '450px',
  },
  previewImage: {
    textAlign: 'center',
  },
  previewContent: {
    color: '#4A4A4A',
    fontFamily: 'Poppins',
    fontWeight: 'bold',
    fontSize: '16px',
    lineHeight: '20px',
    letterSpacing: '-0.06px',
    textAlign: 'center',
  },
  previewButton: {
    textAlign: 'center',
  },
  featuredHeader: {
    color: theme.palette.primary.mediumGray,
    textTransform: 'uppercase',
    fontWeight: 'bold',
    marginTop: 30,
    marginBottom: 10,
    textAlign: 'center',
  },
  title: {
    color: theme.palette.primary.darkGray,
    fontFamily: 'Poppins',
    fontWeight: 'bold',
    fontSize: '38px',
    lineHeight: '47px',
    letterSpacing: '-0.11px',
    textAlign: 'center',
  },
  ticketNumber: {
    color: theme.palette.primary.teal,
    fontFamily: 'Poppins',
    fontWeight: 'bold',
    fontSize: '38px',
    lineHeight: '47px',
    letterSpacing: '-0.11px',
    textAlign: 'center',
    marginTop: 6,
    marginBottom: 20,
  },
  seeGuide: {
    textAlign: 'right',
  },
  tableCell: {
    textDecoration: 'none',
    cursor: 'pointer',
  },
  tabs: {
    marginTop: 40,
    marginBottom: 20,
  },
  tab: {
    width: 140,
  },
  reviewButton: {
    textTransform: 'none',
    width: 180,
    marginBottom: 40,
  },
}));

const columns = [
  {
    id: 'ticketNumber',
    label: 'Ticket #',
    styles: { fontWeight: 'bold' },
  },
  {
    id: 'description',
    label: 'Description',
    minWidth: 170,
    styles: { fontWeight: 'bold' },
  },
  {
    id: 'reason',
    label: 'Reason',
    styles: { fontStyle: 'italic' },
    minWidth: 170,
  },
  {
    id: 'date',
    label: 'Date',
  },
];

export const Start = ({ filter }) => {
  const [currentTab, setCurrentTab] = useState(0);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const { loading, data, refetch } = useQuery(EventsWithDescriptionQuery, {
    onError: error => {
      displayAlert('error', 'Error', error.toString());
    },
  });
  const history = useHistory();
  const styles = useStyles();
  const { displayAlert } = useContext(AlertContext);
  const location = useLocation();
  useEffect(() => {
    const currentPath = location.pathname;
    if (currentPath.endsWith('tickets')) {
      refetch();
    }
  }, [location, refetch]);

  const showCheckOutError = () => {
    displayAlert(
      'info',
      'Ticket unavailable',
      'This ticket has been checked out by another user. It will become available when they check it in or when the lock times out (1 hr).',
    );
  };
  const [checkOutEventMutation] = useMutation(CheckOutEventMutation, {
    onCompleted: data => {
      try {
        const {
          checkOutEvent: {
            event: { id },
          },
        } = data;
        history.push(`/tickets/${id}/review`);
      } catch (e) {
        showCheckOutError();
      }
    },
    onError: error => {
      displayAlert('error', 'Error', error.toString());
      console.log(error);
    },
  });

  const [checkOutEventByIDMutation] = useMutation(CheckOutEventByIDMutation, {
    onCompleted: data => {
      try {
        const {
          checkOutEventById: {
            event: { id },
          },
        } = data;
        history.push(`/tickets/${id}/review`);
      } catch (e) {
        showCheckOutError();
      }
    },
    onError: error => {
      displayAlert('error', 'Error', error.toString());
    },
  });

  const events =
    data && data.eventsWithDescription && data.eventsWithDescription.nodes
      ? data.eventsWithDescription.nodes
      : [];

  const formattedTickets = events
    .map(node => {
      return {
        id: node.id,
        ticketNumber: node.eventNumber,
        description: node.description,
        held: node.isHeld,
        approved: node.isApproved,
        archived: node.isArchived,
        reason: node.reason,
        date: new Date(node.reportedAt).toLocaleString(),
      };
    })
    .sort((ticketA, ticketB) => ticketB.ticketNumber - ticketA.ticketNumber);

  const firstTicket = formattedTickets.length > 0 ? formattedTickets[0] : {};
  let filteredTickets = [];
  if (currentTab === 0) {
    filteredTickets = formattedTickets.filter(
      ticket => !ticket.held && !ticket.approved && !ticket.archived,
    );
  } else if (currentTab === 1) {
    filteredTickets = formattedTickets.filter(ticket => ticket.held);
  } else if (currentTab === 2) {
    filteredTickets = formattedTickets.filter(ticket => ticket.approved);
  } else if (currentTab === 3) {
    filteredTickets = formattedTickets.filter(ticket => ticket.archived);
  }

  const handleTabChange = (event, newValue) => {
    setCurrentTab(newValue);
    refetch();
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const StyledHeaderCell = withStyles(theme => ({
    head: {
      color: theme.palette.common.white,
      backgroundColor: theme.palette.primary.main,
    },
  }))(TableCell);

  return (
    <>
      <Grid container direction="column" justify="center" alignItems="center">
        <Grid item>
          <Typography component="h5" className={styles.featuredHeader}>
            Featured Ticket
          </Typography>
        </Grid>
        <Grid item>
          <Typography className={styles.title} component="h2">
            Start Reviewing
          </Typography>
        </Grid>
        <Grid item>
          <Typography className={styles.ticketNumber} component="h2">
            {`#${firstTicket.ticketNumber || 0}`}
          </Typography>
        </Grid>
        <Grid item>
          <Button
            className={styles.reviewButton}
            variant="contained"
            disableElevation={true}
            color="secondary"
            onClick={e => {
              checkOutEventMutation({
                variables: {
                  clientMutationId: 'test-mutation-id',
                },
              });
            }}
          >
            Review
          </Button>
        </Grid>
      </Grid>

      {loading ? (
        <CircularProgress />
      ) : (
        <Paper>
          <Tabs
            className={styles.tabs}
            value={currentTab}
            onChange={handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            centered
          >
            <Tab label="New" className={styles.tab} />
            <Tab label="Held" className={styles.tab} />
            <Tab label="Approved" className={styles.tab} />
            <Tab label="Rejected" className={styles.tab} />
          </Tabs>
          <TableContainer>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map(column => (
                    <StyledHeaderCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </StyledHeaderCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredTickets
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(row => {
                    return (
                      <TableRow
                        key={row.id}
                        hover
                        tabIndex={-1}
                        onClick={e => {
                          checkOutEventByIDMutation({
                            variables: {
                              eventId: row.id,
                              clientMutationId: 'test-mutation-id',
                            },
                          });
                        }}
                      >
                        {columns.map(column => {
                          const value = row[column.id];
                          return (
                            <TableCell
                              key={column.id}
                              align={column.align}
                              className={styles.tableCell}
                              style={{
                                fontStyle: column.style,
                              }}
                            >
                              {column.format && typeof value === 'number'
                                ? column.format(value)
                                : value}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={filteredTickets.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
      )}
    </>
  );
};
