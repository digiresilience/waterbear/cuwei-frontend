import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import { FieldsV1 } from '@digiresilience/waterbear-fields';

const useStyles = makeStyles((theme) => ({
  tag: {
    margin: theme.spacing(0.5),
    color: 'white',
  },
}));

const allFields = FieldsV1.reduce((acc, field) => {
  if (field.options) {
    acc[field.id] = field.options;
  }
  return acc;
}, {});

export const DisplayTagSet = ({ field, reducer, color }) => {
  const styles = useStyles();
  const tags = field.values;
  const allOptions =
    allFields[field.name] ||
    field.values.map((val) => {
      return { id: val.id, name: val.metadata.name };
    });
  const optionLookup = allOptions.reduce((acc, option) => {
    acc[option.id] = option.name;
    return acc;
  }, {});
  const enabledTags = tags.filter((tag) => tag.valueEdited);
  return enabledTags.map((tag) => (
    <Chip
      key={tag.id}
      label={optionLookup[tag.valueEdited]}
      className={styles.tag}
      style={{
        backgroundColor: '#179e91',
        '&:hover, &:focus': {
          backgroundColor: '#179e91',
        },
      }}
    />
  ));
};
