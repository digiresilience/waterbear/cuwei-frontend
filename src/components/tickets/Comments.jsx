import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  commentData: {
    fontFamily: 'Roboto',
    fontSize: '10px',
    lineHeight: '24px',
    color: '#000000',
    fontWeight: 'bold',
  },
  root: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  gridItem: {
    marginBottom: theme.spacing(2),
  },
  switchContainer: {
    position: 'relative',
    right: '-8px',
  },
  displayContainer: {
    marginBottom: theme.spacing(4),
  },
  held: {
    opacity: '0.25',
    marginBottom: theme.spacing(4),
  },
}));

const EditableComment = ({ comment, reducer }) => {
  const styles = useStyles();
  let d = new Date(comment.metadata.commented_at);

  const handleSwitchChange = event => {
    reducer({ type: 'toggleHeld', payload: { id: comment.id } });
  };

  return (
    <>
      <Typography className={styles.commentData} component="p">
        {comment.metadata.author} {d.toLocaleString()}
      </Typography>
      <Grid container direction="row" justify="space-between">
        <Grid item xs={9} className={styles.gridItem}>
          <TextField fullWidth multiline value={comment.valueEdited} />
        </Grid>
        <Grid item className={styles.switchContainer}>
          <Tooltip
            placement="right"
            title={comment.held ? 'Not sent to database' : 'Sent to database'}
          >
            <Switch color="primary" onChange={handleSwitchChange} />
          </Tooltip>
        </Grid>
      </Grid>
    </>
  );
};

const DisplayComment = ({ comment, reducer }) => {
  const styles = useStyles();
  let d = new Date(comment.metadata.commented_at);

  return (
    <Box className={styles.displayContainer}>
      <Typography variant="body2" className={styles.commentData}>
        {comment.metadata.author} {d.toLocaleString()}
      </Typography>
      <Typography className={comment.held ? styles.held : ''} variant="body1">
        {comment.valueEdited}
      </Typography>
    </Box>
  );
};

export const Comments = ({ field, reducer, editable }) => {
  const comments = field.values || [];
  const styles = useStyles();

  return (
    <>
      <Typography variant="h3">{field.displayName}</Typography>
      {!editable && comments.length === 0 ? (
        <Typography variant="body1" className={styles.held}>
          (No value)
        </Typography>
      ) : (
        undefined
      )}{' '}
      <Box>
        {comments.map((comment, i) =>
          editable ? (
            <EditableComment key={i} comment={comment} reducer={reducer} />
          ) : (
            <DisplayComment key={i} comment={comment} />
          ),
        )}
      </Box>
    </>
  );
};
