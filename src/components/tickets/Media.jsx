import React, { useState, useRef } from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Switch from '@material-ui/core/Switch';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Editor from './ImageEditor.jsx';
import ReplayIcon from '@material-ui/icons/Replay';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  fileName: {
    color: '#4A4A4A',
    overflow: 'scroll',
    fontFamily: 'Roboto',
    fontSize: '14px',
    lineHeight: '20px',
    letterSpacing: '-0.05px',
  },
  mediaItem: {
    padding: theme.spacing(1),
  },
  mediaContainer: {
    marginBottom: '18px',
  },
  editImage: {
    position: 'relative',
    zIndex: 1,
  },
  held: {
    paddingTop: '4px',
    paddingLeft: theme.spacing(2),
    opacity: '0.1',
  },
  // TODO: make top/left calculate based on height/width of image
  editButton: {
    position: 'absolute',
    top: '50px',
    left: '70px',
    zIndex: 3,
  },
  modal: {
    position: 'absolute',
    width: 800,
    backgroundColor: '#1f1e1f',
    color: 'white',
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 1, 3),
  },
}));

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

export const Media = (props) => {
  const { field, reducer, editable } = props;
  const attachments = field.values || [];
  const styles = useStyles();
  const [open, setOpen] = useState(false);
  const [currentAttachment, setCurrentAttachment] = useState({
    valueEdited: '',
    metadata: {},
  });
  const [modalStyle] = useState(getModalStyle);
  const editorRef = useRef(null);

  const handleOpen = (attachment) => {
    setCurrentAttachment(attachment);
    setOpen(true);
  };

  const handleSave = () => {
    const editor = editorRef.current.getInstance();
    const dataValue = editor.toDataURL();

    if (dataValue) {
      const value = dataValue.split(',')[1];
      reducer({
        type: 'updateValue',
        payload: { id: currentAttachment.id, value },
      });
    }
    setOpen(false);
  };

  const handleRevert = (tagType) => {
    reducer({ type: 'revertValue', payload: { id: currentAttachment.id } });
    setOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const MediaItem = ({ attachment, reducer, editable }) => {
    const styles = useStyles();

    const handleSwitchChange = (id) => {
      reducer({ type: 'toggleHeld', payload: { id } });
    };

    return (
      <Grid item xs={4}>
        <Grid container direction="row">
          <Grid item>
            <div className={styles.editImage}>
              <img
                width="100%"
                src={`data:${attachment.metadata.mimetype};base64, ${attachment.valueEdited}`}
                alt="attachment for editing"
                className={
                  !editable && attachment.held ? styles.held : undefined
                }
              />
              {editable ? (
                <div className="edit-button">
                  <Button
                    variant="contained"
                    className={styles.editButton}
                    onClick={() => handleOpen(attachment)}
                  >
                    Edit
                  </Button>
                </div>
              ) : null}
            </div>
          </Grid>
          <Grid item xs={9}>
            <Typography className={styles.fileName}>
              {attachment.metadata.filename}
            </Typography>
          </Grid>
          {editable ? (
            <Grid item xs={2}>
              <Tooltip
                placement="bottom"
                title={
                  attachment.held ? 'Not sent to database' : 'Sent to database'
                }
              >
                <Switch
                  color="primary"
                  checked={!attachment.held}
                  onChange={() => handleSwitchChange(attachment.id)}
                />
              </Tooltip>
            </Grid>
          ) : null}
        </Grid>
      </Grid>
    );
  };

  return (
    <>
      <Typography variant="h3">{field.displayName}</Typography>
      <Grid
        container
        direction="row"
        spacing={4}
        className={styles.mediaContainer}
      >
        {!editable && attachments.length === 0 ? (
          <Typography variant="body1" className={styles.held}>
            (No value)
          </Typography>
        ) : undefined}
        {attachments.map((attachment) => {
          return (
            <MediaItem
              key={attachment.id}
              className={styles.mediaItem}
              attachment={attachment}
              editable={editable}
              reducer={reducer}
            />
          );
        })}
      </Grid>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <div style={modalStyle} className={styles.modal}>
          <Grid container direction="row" alignItems="center">
            <Grid item xs={9}>
              <Box ml={4}>
                <h2 id="simple-modal-title">
                  {currentAttachment.metadata.filename}
                </h2>
              </Box>
            </Grid>
            <Grid item xs={1}>
              <Button
                variant="contained"
                style={{ backgroundColor: 'white' }}
                onClick={handleRevert}
              >
                <ReplayIcon />
              </Button>
            </Grid>
            <Grid item xs={1}>
              <Button
                variant="contained"
                style={{ backgroundColor: 'white' }}
                onClick={handleSave}
              >
                Save
              </Button>
            </Grid>
            <Grid item style={{ textAlign: 'right' }} xs={1}>
              <IconButton onClick={handleClose} style={{ color: 'white' }}>
                <CloseIcon />
              </IconButton>
            </Grid>
          </Grid>
          <Editor
            imageUri={`data:${currentAttachment.metadata.mimetype};base64, ${currentAttachment.valueEdited}`}
            imageName={currentAttachment.metadata.filename}
            editorRef={editorRef}
          />
        </div>
      </Modal>
    </>
  );
};
