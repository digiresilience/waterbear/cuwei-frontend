import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { FieldsV1 } from '@digiresilience/waterbear-fields';

const useStyles = makeStyles((theme) => ({
  tagSection: {
    width: '100%',
  },
  sectionTitle: {
    marginBottom: theme.spacing(2),
  },
  primaryButton: {
    color: theme.palette.primary.teal,
  },
  original: {
    display: 'flex',
    justifyContent: 'left',
    flexWrap: 'wrap',
    paddingTop: theme.spacing(1),
  },
  originalText: {
    fontFamily: 'Roboto',
    fontSize: '14px',
    lineHeight: '18px',
    color: '#4A4A4A',
    fontStyle: 'italic',
    marginTop: theme.spacing(2),
  },
  divider: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  tag: {
    margin: theme.spacing(0.5),
    color: 'white',
  },
}));

let allFields = FieldsV1.reduce((acc, field) => {
  if (field.options) {
    acc[field.id] = field.options;
  }
  return acc;
}, {});

export const EditableTagSet = ({ field, reducer, color, last }) => {
  const styles = useStyles();
  const tags = field.values;
  const [edited, setEdited] = React.useState(false);
  const [showOriginal, setShowOriginal] = React.useState(false);
  const allOptions =
    allFields[field.name] ||
    field.values.map((val) => {
      return { id: val.id, name: val.metadata.name };
    });

  const selectedOptions = tags.map((tag) => tag.valueEdited);
  const originalSelectedOptions = tags
    .filter((tag) => !!tag.id)
    .map((tag) => tag.value);

  const handleChipClick = (value) => {
    reducer({ type: 'toggleOptionValue', payload: { value } });
    setEdited(true);
  };

  const handleRevertClick = (tagType) => {
    reducer({ type: 'revertOptionValues', payload: {} });
    setShowOriginal(false);
    setEdited(false);
  };

  return (
    <Box key={field.displayName} display="block" className={styles.tagSection}>
      <Grid container direction="row" justify="space-between">
        <Grid item>
          <Typography variant="h4" className={styles.sectionTitle}>
            {field.displayName}
          </Typography>
        </Grid>
        <Grid item>
          <Grid container direction="row" alignItems="center">
            {showOriginal && originalSelectedOptions.length === 0 && (
              <Grid item>
                <Typography
                  variant="caption"
                  style={{ color: '#9a9a9a', marginRight: 10 }}
                >
                  No originals selected
                </Typography>
              </Grid>
            )}
            {showOriginal && (
              <Grid item>
                <Button
                  onClick={handleRevertClick}
                  classes={{ text: styles.primaryButton }}
                >
                  Revert
                </Button>
              </Grid>
            )}
            {showOriginal && (
              <Grid item>
                <Button onClick={() => setShowOriginal(false)}>Done</Button>
              </Grid>
            )}
            {edited && !showOriginal && (
              <Grid item>
                <Button
                  onClick={() => setShowOriginal(true)}
                  classes={{ text: styles.primaryButton }}
                  style={{ color: '#3701b3' }}
                >
                  Show Original
                </Button>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
      <Box display="block">
        {allOptions.map((option) => {
          const selected = selectedOptions.includes(option.id);
          const original = originalSelectedOptions.includes(option.id);

          return (
            <Chip
              key={option.id}
              label={option.name}
              onClick={() => handleChipClick(option.id)}
              className={styles.tag}
              style={{
                border: showOriginal && original ? '3px solid #3701b3' : '0px',
                backgroundColor: selected ? '#179e91' : '#bbb',
                '&:hover, &:focus': {
                  backgroundColor: selected ? '#179e91' : '#bbb',
                },
              }}
            />
          );
        })}
      </Box>
      {!last ? <Divider className={styles.divider} /> : null}
    </Box>
  );
};
