import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { useParams } from 'react-router-dom';
import { UserDetail } from './UserDetail';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import { UsersQuery } from '../../queries/admin/UsersQuery';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { withStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';

const columns = [
  {
    id: 'name',
    label: 'Name',
    styles: { fontWeight: 'bold' },
  },
  {
    id: 'email',
    label: 'Email',
    minWidth: 170,
    styles: { fontWeight: 'bold' },
  },
  {
    id: 'userRole',
    label: 'Role',
  },
  {
    id: 'isActive',
    label: 'Active',
    minWidth: 170,
  },
  {
    id: 'createdAt',
    label: 'Created At',
  },
];

export const Users = (props) => {
  const history = useHistory();
  const { id } = useParams();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const { loading, data, error, refetch } = useQuery(UsersQuery);

  const onHide = () => {
    history.push('/admin/users');
    refetch();
  };

  if (loading) {
    return <CircularProgress />;
  }

  if (error) {
    console.log(error);
    return <div>Error</div>;
  }

  const {
    users: { nodes: users },
  } = data;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const StyledHeaderCell = withStyles((theme) => ({
    head: {
      color: theme.palette.common.white,
      backgroundColor: theme.palette.primary.main,
    },
  }))(TableCell);

  return (
    <Container>
      {id && <UserDetail visible={true} onHide={onHide} />}
      <Grid container justify="space-between" alignItems="center">
        <Grid item>
          <h1>Users</h1>
        </Grid>
        <Grid item>
          <Button
            href="/#/admin/users/create"
            variant="contained"
            color="secondary"
          >
            Create User
          </Button>
        </Grid>
      </Grid>
      <Paper>
        <TableContainer>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <StyledHeaderCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </StyledHeaderCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {users
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      component={Link}
                      href={`/#/admin/users/${row.id}`}
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.id}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell
                            key={column.id}
                            align={column.align}
                            style={{ fontStyle: column.style }}
                          >
                            {value.toString()}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={users.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </Container>
  );
};
