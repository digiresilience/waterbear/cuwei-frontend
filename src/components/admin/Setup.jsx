import React, { useState } from 'react';
import { Typography } from '@material-ui/core';
import { Button } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { useMutation } from '@apollo/react-hooks';
import { CreateFirstUserMutation } from '../../queries/admin/CreateFirstUserMutation';

export const Setup = () => {
  const [name, setName] = useState('');
  const [emailAddress, setEmailAddress] = useState('');

  const [createFirstUser] = useMutation(CreateFirstUserMutation, {
    onCompleted: () => {},
    onError: error => {
      return error;
    },
  });

  return (
    <>
      <Paper style={{ width: 400 }}>
        <Typography variant="h5">Create First User</Typography>
      </Paper>
      <Grid
        container
        direction="column"
        spacing={1}
        style={{ width: 400, padding: 20 }}
      >
        <Grid item>
          <TextField
            value={name}
            onChange={e => setName(e.target.value)}
            placeholder="Name"
          />
        </Grid>
        <Grid item>
          <TextField
            value={emailAddress}
            onChange={e => setEmailAddress(e.target.value)}
            placeholder="Email Address"
          />
        </Grid>
        <Grid item>
          <Grid container justify="space-between">
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  createFirstUser({
                    variables: {
                      input: {
                        userEmail: emailAddress,
                        userName: name,
                      },
                    },
                  });
                }}
              >
                Create
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
