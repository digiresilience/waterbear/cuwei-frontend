import React from 'react';
import { Route } from 'react-router-dom';
import { Users } from './Users.jsx';
import { Setup } from './Setup.jsx';

export const Admin = (props) => {
  return (
    <div>
      <Route path="/admin/setup">
        <Setup />
      </Route>
      <Route path="/admin/users/:id">
        <Users />
      </Route>
      <Route exact path="/admin/users">
        <Users />
      </Route>
    </div>
  );
};
