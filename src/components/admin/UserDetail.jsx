import React, { useState } from 'react';
import { Typography } from '@material-ui/core';
import { Button } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@material-ui/core';
import { MenuItem } from '@material-ui/core';
import { Formik, Form, Field } from 'formik';
import { TextField, Select } from 'formik-material-ui';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { CreateUserMutation } from '../../queries/admin/CreateUserMutation';
import { UpdateUserMutation } from '../../queries/admin/UpdateUserMutation';
import { CurrentUserQuery } from '../../queries/common/CurrentUserQuery.js';
import { GetUserQuery } from '../../queries/admin/GetUserQuery';
import { makeStyles } from '@material-ui/core/styles';
import { useParams } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  field: { marginTop: 10 },
  select: { marginTop: 24 },
}));

export const UserDetail = ({ visible, onHide }) => {
  const { id } = useParams();
  const isCreate = id === 'create';
  const [user, setUser] = useState(null);
  const [currentUserData, setCurrentUserData] = useState({
    email: 'loading',
  });
  const styles = useStyles();

  useQuery(GetUserQuery, {
    variables: { id },
    onCompleted: (data) => {
      if (data && data.user) {
        setUser(data.user);
      }
    },
    onError: (error) => {
      console.log(error);
    },
  });

  useQuery(CurrentUserQuery, {
    onCompleted: (data) => {
      if (data && data.currentUser) {
        setCurrentUserData(data.currentUser);
      }
    },
    onError: (error) => {
      console.log(error);
    },
  });

  const [createUser] = useMutation(CreateUserMutation, {
    onCompleted: () => {},
    onError: (error) => {
      return error;
    },
  });

  const [updateUser] = useMutation(UpdateUserMutation, {
    onCompleted: () => {},
    onError: (error) => {
      return error;
    },
  });

  const allRoles = [
    { label: 'Admin', value: 'ADMIN' },
    { label: 'Senior Core Analyst', value: 'SENIOR_CORE_ANALYST' },
    { label: 'Core Analyst', value: 'CORE_ANALYST' },
    { label: 'Investigator', value: 'INVESTIGATOR' },
  ];

  const createOrUpdateUser = async (values) => {
    if (isCreate) {
      await createUser({
        variables: {
          input: {
            user: {
              isActive: true,
              createdBy: currentUserData.email,
              ...values,
            },
          },
        },
      });
    } else {
      await updateUser({ variables: { input: { id, patch: values } } });
    }
  };

  return (
    <Dialog open={visible}>
      <DialogTitle>
        <Typography variant="h5">
          {isCreate ? 'Create User' : 'Update User'}
        </Typography>
      </DialogTitle>
      {(isCreate || user) && (
        <Formik
          initialValues={{
            name: user ? user.name : '',
            email: user ? user.email : '',
            userRole: user ? user.userRole : 'INVESTIGATOR',
            filterTags: user ? user.filterTags : '',
          }}
          validate={(values) => {
            const errors = {};
            return errors;
          }}
          onSubmit={async (values, { setSubmitting }) => {
            await createOrUpdateUser(values);
            await onHide();
          }}
        >
          {({ submitForm, isSubmitting }) => (
            <Form>
              <DialogContent
                style={{ padding: '0px 24px 24px 24px', maxWidth: 500 }}
              >
                <Field
                  component={TextField}
                  name="name"
                  label="Name"
                  className={styles.field}
                  fullWidth
                />
                <Field
                  component={TextField}
                  name="email"
                  label="Email Address"
                  className={styles.field}
                  fullWidth
                />
                <Field
                  component={Select}
                  name="userRole"
                  inputProps={{ id: 'userRole' }}
                  className={styles.select}
                  fullWidth
                >
                  {allRoles.map((role) => {
                    return <MenuItem value={role.value}>{role.label}</MenuItem>;
                  })}
                </Field>
                <Field
                  component={TextField}
                  name="filterTags"
                  label="Filter Tags"
                  placeholder="comma separated"
                  className={styles.field}
                  fullWidth
                />
              </DialogContent>
              <DialogActions>
                <Grid container justify="space-between" style={{ padding: 24 }}>
                  <Grid item>
                    <Button href="/#/admin/users" variant="contained">
                      Cancel
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={submitForm}
                    >
                      Save
                    </Button>
                  </Grid>
                </Grid>
              </DialogActions>
            </Form>
          )}
        </Formik>
      )}
    </Dialog>
  );
};
