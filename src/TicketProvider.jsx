import React, { createContext, useState, useReducer } from 'react';
import produce from 'immer';

export const TicketContext = createContext({});

const reducer = (state, action) => {
  const indexForId = (id) => {
    return id ? state.values.findIndex((val) => id === val.id) : 0;
  };
  const indexForValue = (value) => {
    return value ? state.values.findIndex((val) => value === val.value) : -1;
  };

  if (action.type === 'updateField') {
    const { attribute } = action.payload;
    const nextState = produce(state, (draftState) => {
      if (state.default) {
        draftState.default = false;
        draftState.values = [];
      }
      draftState.values.push({
        id: attribute.id,
        type: attribute.field.type,
        edited: false,
        held: attribute.isHeld,
        metadata: attribute.metadata,
        value: attribute.value,
        valueEdited: attribute.valueEdited || attribute.value,
      });
    });
    return nextState;
  } else if (action.type === 'updateOptionField') {
    const { attribute } = action.payload;
    const nextState = produce(state, (draftState) => {
      if (state.default) {
        draftState.default = false;
        draftState.values = [];
      }
      const selectedOption = attribute.field.fieldOptions.nodes.find(
        (option) => {
          return option.id === attribute.value;
        },
      );
      draftState.values.push({
        id: attribute.id,
        type: attribute.field.type,
        edited: false,
        held: attribute.isHeld,
        metadata: attribute.metadata,
        value: selectedOption ? selectedOption.name : null,
        valueEdited: selectedOption ? selectedOption.name : null,
      });
    });
    return nextState;
  } else if (action.type === 'updateTagField') {
    const { tags } = action.payload;
    const nextState = produce(state, (draftState) => {
      if (state.default) {
        draftState.default = true;
        draftState.values = [];
      }

      const allTags = tags.map((tag) => ({
        id: tag.id,
        type: 'text',
        edited: false,
        held: false,
        metadata: { name: tag.displayName },
        value: tag.id,
        valueEdited: tag.id,
      }));
      draftState.values = allTags;
    });
    return nextState;
  } else if (action.type === 'updateValue') {
    const { id, value } = action.payload;
    const index = indexForId(id);
    const nextState = produce(state, (draftState) => {
      draftState.values[index].valueEdited = value;
      draftState.values[index].edited = true;
    });
    return nextState;
  } else if (action.type === 'revertValue') {
    const { id } = action.payload;
    const index = indexForId(id);
    const nextState = produce(state, (draftState) => {
      draftState.values[index].valueEdited = state.values[index].value;
      draftState.values[index].edited = false;
    });
    return nextState;
  } else if (action.type === 'toggleOptionValue') {
    const { value } = action.payload;
    const index = indexForValue(value);
    const nextState = produce(state, (draftState) => {
      if (index > -1) {
        const valueEdited = draftState.values[index].valueEdited;
        if (valueEdited) {
          draftState.values[index].valueEdited = null;
          draftState.values[index].edited = true;
          draftState.values[index].held = true;
        } else {
          draftState.values[index].valueEdited = value;
          draftState.values[index].edited = false;
          draftState.values[index].held = false;
        }
      } else {
        draftState.values.push({
          id: null,
          type: null,
          edited: false,
          held: false,
          metadata: null,
          value: value,
          valueEdited: value,
        });
      }
    });
    return nextState;
  } else if (action.type === 'revertOptionValues') {
    const nextState = produce(state, (draftState) => {
      const updatedValues = draftState.values
        .filter((val) => !!val.id)
        .map((val) => {
          val.valueEdited = val.value;
          val.edited = false;
          return val;
        });
      draftState.values = updatedValues;
    });
    return nextState;
  } else if (action.type === 'toggleHeld') {
    const { id } = action.payload;
    const index = indexForId(id);
    const nextState = produce(state, (draftState) => {
      draftState.values[index].held = !state.values[index].held;
      draftState.values[index].edited = true;
    });
    return nextState;
  } else if (action.type === 'reset') {
    const { empty } = action.payload;
    const nextState = produce(state, (draftState) => {
      draftState.default = true;
      draftState.values = [];

      if (!empty) {
        draftState.values.push({
          edited: false,
          held: false,
          value: '',
          valueEdited: '',
          metadata: {},
          type: '',
          id: '',
        });
      }
    });
    return nextState;
  }
};

const stateDefaults = {
  default: true,
  values: [
    {
      edited: false,
      held: true,
      value: '',
      valueEdited: '',
      metadata: {},
      id: '',
    },
  ],
};

const emptyStateDefaults = {
  default: true,
  values: [],
};

export const TicketProvider = ({ children }) => {
  const [ticketID, setTicketID] = useState(0);
  const [ticketNumber, setTicketNumber] = useState(0);
  const [edited, setEdited] = useState(false);
  const [description, descriptionDispatch] = useReducer(reducer, {
    displayName: 'What was seen or heard? *',
    ...stateDefaults,
  });
  const [attachments, attachmentsDispatch] = useReducer(reducer, {
    displayName: 'Media',
    ...emptyStateDefaults,
  });
  const [comments, commentsDispatch] = useReducer(reducer, {
    displayName: 'Analyst comments',
    ...emptyStateDefaults,
  });
  const [links, linksDispatch] = useReducer(reducer, {
    displayName: 'Links',
    ...emptyStateDefaults,
  });
  const [date, dateDispatch] = useReducer(reducer, {
    displayName: 'When did it happen?',
    ...stateDefaults,
  });
  const [geography, geographyDispatch] = useReducer(reducer, {
    displayName: 'Where did it happen?',
    ...stateDefaults,
  });
  const [reason, reasonDispatch] = useReducer(reducer, {
    displayName: "What's suspicious about it?",
    ...stateDefaults,
  });
  const [medium, mediumDispatch] = useReducer(reducer, {
    displayName: 'How did you come across it? Check all that apply.',
    ...stateDefaults,
  });
  const [additionalInfo, additionalInfoDispatch] = useReducer(reducer, {
    displayName: 'Have advanced information about the account in question?',
    ...stateDefaults,
  });
  const [sensitivity, sensitivityDispatch] = useReducer(reducer, {
    displayName: 'Security level',
    ...stateDefaults,
  });
  const [tactics, tacticsDispatch] = useReducer(reducer, {
    name: 'tactic',
    displayName: 'Tactics',
    ...emptyStateDefaults,
  });
  const [platforms, platformsDispatch] = useReducer(reducer, {
    name: 'platform',
    displayName: 'Platforms',
    ...emptyStateDefaults,
  });
  const [parties, partiesDispatch] = useReducer(reducer, {
    name: 'party',
    displayName: 'Parties',
    ...emptyStateDefaults,
  });
  const [candidates, candidatesDispatch] = useReducer(reducer, {
    name: 'candidate',
    displayName: 'Candidates',
    ...emptyStateDefaults,
  });
  const [actors, actorsDispatch] = useReducer(reducer, {
    name: 'actor',
    displayName: 'Actors',
    ...emptyStateDefaults,
  });
  const [communities, communitiesDispatch] = useReducer(reducer, {
    name: 'communities',
    displayName: 'Communities',
    ...emptyStateDefaults,
  });
  const [customTags, customTagsDispatch] = useReducer(reducer, {
    name: 'customTags',
    displayName: 'Custom Tags',
    ...emptyStateDefaults,
  });

  const resetTicket = () => {
    setTicketID(0);
    setTicketNumber(0);
    setEdited(false);
    descriptionDispatch({ type: 'reset', payload: { empty: false } });
    attachmentsDispatch({ type: 'reset', payload: { empty: true } });
    commentsDispatch({ type: 'reset', payload: { empty: true } });
    linksDispatch({ type: 'reset', payload: { empty: false } });
    dateDispatch({ type: 'reset', payload: { empty: false } });
    geographyDispatch({ type: 'reset', payload: { empty: false } });
    reasonDispatch({ type: 'reset', payload: { empty: false } });
    mediumDispatch({ type: 'reset', payload: { empty: false } });
    additionalInfoDispatch({ type: 'reset', payload: { empty: false } });
    sensitivityDispatch({ type: 'reset', payload: { empty: false } });
    tacticsDispatch({ type: 'reset', payload: { empty: true } });
    platformsDispatch({ type: 'reset', payload: { empty: true } });
    partiesDispatch({ type: 'reset', payload: { empty: true } });
    candidatesDispatch({ type: 'reset', payload: { empty: true } });
    actorsDispatch({ type: 'reset', payload: { empty: true } });
    communitiesDispatch({ type: 'reset', payload: { empty: true } });
    customTagsDispatch({ type: 'reset', payload: { empty: true } });
  };

  const setTicket = (ticket) => {
    if (ticket.id === ticketID) {
      return;
    }

    resetTicket();
    setTicketID(ticket.id);
    setTicketNumber(ticket.eventNumber);
    setEdited(false);
    customTagsDispatch({
      type: 'updateTagField',
      payload: { tags: ticket.tags.nodes },
    });

    for (const attribute of ticket.attributes.nodes) {
      const fieldName = attribute.field.name;
      switch (fieldName) {
        case 'description':
          descriptionDispatch({ type: 'updateField', payload: { attribute } });
          break;
        case 'attachments':
          attachmentsDispatch({ type: 'updateField', payload: { attribute } });
          break;
        case 'comments':
          commentsDispatch({ type: 'updateField', payload: { attribute } });
          break;
        case 'disinfo_links':
          linksDispatch({ type: 'updateField', payload: { attribute } });
          break;
        case 'sighted_on':
          dateDispatch({ type: 'updateField', payload: { attribute } });
          break;
        case 'reason':
          reasonDispatch({ type: 'updateField', payload: { attribute } });
          break;
        case 'medium':
          mediumDispatch({ type: 'updateOptionField', payload: { attribute } });
          break;
        case 'additional_info':
          additionalInfoDispatch({
            type: 'updateField',
            payload: { attribute },
          });
          break;
        case 'sensitivity':
          sensitivityDispatch({ type: 'updateField', payload: { attribute } });
          break;
        case 'tactic':
          tacticsDispatch({
            type: 'updateOptionField',
            payload: { attribute },
          });
          break;
        case 'platform':
          platformsDispatch({
            type: 'updateOptionField',
            payload: { attribute },
          });
          break;
        case 'party':
          partiesDispatch({
            type: 'updateOptionField',
            payload: { attribute },
          });
          break;
        case 'candidate':
          candidatesDispatch({
            type: 'updateOptionField',
            payload: { attribute },
          });
          break;
        case 'actor':
          actorsDispatch({ type: 'updateOptionField', payload: { attribute } });
          break;
        case 'communities':
          communitiesDispatch({
            type: 'updateOptionField',
            payload: { attribute },
          });
          break;
        default:
          break;
      }
    }
  };

  const formatAttribute = (att) => {
    const editedValues = att.values
      .filter((val) => val.edited)
      .map((val) => ({
        id: val.id,
        isHeld: val.held,
        valueEdited: val.valueEdited,
      }));
    return editedValues.length > 0 ? editedValues : null;
  };

  const stateToGraphQL = (action, message) => {
    let attrs = [
      formatAttribute(description),
      formatAttribute(attachments),
      formatAttribute(comments),
      formatAttribute(links),
      formatAttribute(date),
      formatAttribute(geography),
      formatAttribute(reason),
      formatAttribute(medium),
      formatAttribute(additionalInfo),
      formatAttribute(sensitivity),
      formatAttribute(tactics),
      formatAttribute(platforms),
      formatAttribute(parties),
      formatAttribute(candidates),
      formatAttribute(actors),
      formatAttribute(communities),
    ]
      .flat()
      .filter((a) => a);

    const formattedTags = formatAttribute(customTags);
    const tags = formattedTags
      ? formattedTags.map((val) => {
          delete val.valueEdited;
          return val;
        })
      : [];

    const out = {
      eventId: ticketID,
      clientMutationId: 'client-mutation-id',
      action: action.toUpperCase(),
      reason: message,
      attrs,
      tags,
    };

    return out;
  };

  return (
    <TicketContext.Provider
      value={{
        ticketID,
        ticketNumber,
        edited,
        description,
        descriptionDispatch,
        attachments,
        attachmentsDispatch,
        links,
        linksDispatch,
        comments,
        commentsDispatch,
        date,
        dateDispatch,
        geography,
        geographyDispatch,
        reason,
        reasonDispatch,
        medium,
        mediumDispatch,
        additionalInfo,
        additionalInfoDispatch,
        sensitivity,
        sensitivityDispatch,
        tactics,
        tacticsDispatch,
        platforms,
        platformsDispatch,
        parties,
        partiesDispatch,
        candidates,
        candidatesDispatch,
        actors,
        actorsDispatch,
        communities,
        communitiesDispatch,
        customTags,
        customTagsDispatch,
        setTicket,
        stateToGraphQL,
      }}
    >
      {children}
    </TicketContext.Provider>
  );
};
