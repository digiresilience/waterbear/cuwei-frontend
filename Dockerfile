FROM node:13-stretch as builder
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
ARG CUWEI_DIR=/cuwei-frontend
RUN mkdir -p ${CUWEI_DIR}/
COPY package.json ${CUWEI_DIR}/
COPY public ${CUWEI_DIR}/public
COPY src ${CUWEI_DIR}/src
COPY .npmrc ${CUWEI_DIR}/
RUN chown -R node:node ${CUWEI_DIR}/
USER node
WORKDIR ${CUWEI_DIR}
RUN yarn install
RUN rm -f .npmrc
RUN yarn build

FROM nginx
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL="https://gitlab.com/digiresilience/waterbear/cuwei-frontend"
ARG VERSION
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="digiresilience/cuwei"
LABEL org.label-schema.description=""
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.vcs-url=$VCS_URL
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.version=$VERSION
ARG CUWEI_DIR=/cuwei-frontend
ENV CUWEI_DIR ${CUWEI_DIR}

COPY --from=builder ${CUWEI_DIR}/build /usr/share/nginx/html
EXPOSE 80
